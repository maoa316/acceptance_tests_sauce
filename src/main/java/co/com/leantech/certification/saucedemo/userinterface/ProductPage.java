package co.com.leantech.certification.saucedemo.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;


public class ProductPage extends PageObject {

    public static  final Target LABEL_PRODUCTS_TITLE = Target.the("Text containing the title of the page")
            .located(By.xpath("//*[@id='header_container']/div[2]/span"));
    public static final Target SELECT_PRODUCT_FILTER =  Target.the("This select allows filter the products")
        .located(By.className("product_sort_container"));
    public static final Target BUTTON_FIRST_PRODUCT = Target.the("Add to cart button allows add the first product of the current list to the shopping cart")
            .located(By.xpath("(//button[contains(text(), 'Add to cart')])[1]"));
    public static final Target BUTTON_SHOPING_CART = Target.the("Shopping cart button allows you to continue with the purchase")
            .located(By.id("shopping_cart_container"));
    public static final Target  BUTTON_CHECKOUT =  Target.the("Checkout button allow checkout the purchase")
            .located(By.id("checkout"));
    public static final Target INPUT_FIRST_NAME = Target.the("First Name input to write the firstname in the checkout purchase")
            .located(By.id("first-name"));
    public static final Target INPUT_LAST_NAME = Target.the("Last Name input to write the lastname in the checkout purchase")
            .located(By.id("last-name"));
    public static final Target INPUT_POSTAL_CODE = Target.the("Postal Code input to write the postal code in the checkout purchase")
            .located(By.id("postal-code"));
    public static final Target BUTTON_CONTINUE_CHECKOUT = Target.the("Continue button allow continue the checkout information")
            .located(By.id("continue"));
    public static final Target BUTTON_FINISH_CHECKOUT = Target.the("Finish button allow finish the checkout purchase")
            .located(By.id("finish"));
    public static final Target LABEL_CHECKOUT_COMPLETE = Target.the("The Text text that contains the complete purchase")
            .located(By.xpath("//*[@id='header_container']/div[2]/span"));
}
