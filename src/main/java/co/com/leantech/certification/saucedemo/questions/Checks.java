package co.com.leantech.certification.saucedemo.questions;

import co.com.leantech.certification.saucedemo.userinterface.LoginUserPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.leantech.certification.saucedemo.userinterface.ProductPage.*;

public class Checks implements Question<Boolean> {

    private String strExpectedResults;

    public Checks(String strExpectedResults){
        this.strExpectedResults = strExpectedResults;
    }

    public static Checks expectedResults(String strExpectedResults) {
        return new Checks(strExpectedResults);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean boolResult = false;
        String strText;

        if(strExpectedResults.equalsIgnoreCase("products")){
            strText = Text.of(LABEL_PRODUCTS_TITLE).viewedBy(actor).asString();
        }else{
            strText = Text.of(LoginUserPage.LABEL_ERROR_LOGIN).viewedBy(actor).asString();
        }

        if(strExpectedResults.equalsIgnoreCase(strText)){
            boolResult = true;
        }

        return boolResult;
    }
}
