package co.com.leantech.certification.saucedemo.questions;

import co.com.leantech.certification.saucedemo.userinterface.ProductPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Checkout implements Question<String> {

    public static Checkout thePurchase() {
        return new Checkout();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(ProductPage.LABEL_CHECKOUT_COMPLETE).viewedBy(actor).asString();
    }
}
