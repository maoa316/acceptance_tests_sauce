package co.com.leantech.certification.saucedemo.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.leantech.certification.saucedemo.userinterface.LoginUserPage.*;


public class Login implements Task {

    private String strUserName;
    private String strPassword = "secret_sauce";

    public Login(String strUserName){
        this.strUserName = strUserName;
    }

    public static Login withTheUser(String strUserName) {
        return Tasks.instrumented(Login.class, strUserName);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.remember("username", strUserName);
        actor.attemptsTo(
                Enter.theValue(strUserName).into(INPUT_USER_NAME),
                Enter.theValue(strPassword).into(INPUT_PASSWORD),
                Click.on(BUTTON_LOGIN)
        );
    }
}
