package co.com.leantech.certification.saucedemo.model.enums;

public enum Environments {


    TEST_SWAGLABS("SWAGLABS","https://www.saucedemo.com/");

    private final String strName;
    private final String strUrl;

    Environments(String strName, String strUrl){
        this.strName = strName;
        this.strUrl = strUrl;
    }

    public String getStrName() {
        return strName;
    }

    public String getStrUrl() {
        return strUrl;
    }
}
