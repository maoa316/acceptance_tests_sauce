package co.com.leantech.certification.saucedemo.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoginUserPage extends PageObject {

    public static final Target INPUT_USER_NAME = Target.the("user name input to write the user name")
            .located(By.id("user-name"));
    public static final Target INPUT_PASSWORD = Target.the(" password input to write the password")
            .located(By.id("password"));
    public static final Target BUTTON_LOGIN = Target.the("Login button to enter the saucedemo page")
            .located(By.id("login-button"));
    public static final Target LABEL_ERROR_LOGIN = Target.the("the text of the label error").
            locatedBy("//*[@id='login_button_container']/div/form/div[3]/h3");
}
