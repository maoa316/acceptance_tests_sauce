package co.com.leantech.certification.saucedemo.tasks;


import co.com.leantech.certification.saucedemo.model.PersonalDataModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;

import java.util.List;

import static co.com.leantech.certification.saucedemo.userinterface.ProductPage.*;

public class Purchase implements Task {

    private List<PersonalDataModel> personalData;

    public Purchase(List<PersonalDataModel> personalData){
        this.personalData = personalData;
    }

    public static Purchase theProducts(List<PersonalDataModel> personalData) {
        return Tasks.instrumented(Purchase.class, personalData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        PersonalDataModel personalDataCheckout= personalData.get(0);
        actor.attemptsTo(
                SelectFromOptions.byValue("hilo").from(SELECT_PRODUCT_FILTER),
                Click.on(BUTTON_FIRST_PRODUCT),
                SelectFromOptions.byValue("lohi").from(SELECT_PRODUCT_FILTER),
                Click.on(BUTTON_FIRST_PRODUCT),
                Click.on(BUTTON_SHOPING_CART),
                Click.on(BUTTON_CHECKOUT),
                Enter.theValue(personalDataCheckout.getFirstName()).into(INPUT_FIRST_NAME),
                Enter.theValue(personalDataCheckout.getLastName()).into(INPUT_LAST_NAME),
                Enter.theValue(personalDataCheckout.getZipCode()).into(INPUT_POSTAL_CODE),
                Click.on(BUTTON_CONTINUE_CHECKOUT),
                Click.on(BUTTON_FINISH_CHECKOUT),
                Scroll.to(LABEL_CHECKOUT_COMPLETE)
        );
    }
}
