package co.com.leantech.certification.saucedemo.tasks;

import co.com.leantech.certification.saucedemo.userinterface.LoginUserPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Initialize  implements Task {

    private LoginUserPage loginUserPage;

    public static Initialize theEnvironment(){
        return Tasks.instrumented(Initialize.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(loginUserPage));
    }
}
