@stories
Feature: Test the funcionality of the Sauce Demo
  As a user, I Want to test the funcionality of the website

  @e2eSauceDemo
  Scenario Outline: Test the login functionality
    Given than mauricio wants enter the saucedemo website
    When  enter the system with the username <username>
    Then he see the <expected result> text on the current page
    Examples:
      |username                |expected result                                                           |
      |standard_user           |PRODUCTS                                                                  |
      |locked_out_user         |Epic sadface: Sorry, this user has been locked out.                       |
      |problem_user            |PRODUCTS                                                                  |
      |performance_glitch_user |PRODUCTS                                                                  |
      |undefined_user          |Epic sadface: Username and password do not match any user in this service |

  @e2eSauceDemo
  Scenario Outline: Test the purchase lowest and highest priced product
    Given than mauricio wants enter the saucedemo website
    When  enter the system with the username standard_user
    And he want purchase the lowest and highest priced products
    |firstName   |lastName   |zipCode   |
    |<firstName> |<lastName> |<zipCode> |
    Then he see the Checkout: Complete!
    Examples:
      |firstName |lastName |zipCode |
      |Mauricio  |Agudelo  |054     |