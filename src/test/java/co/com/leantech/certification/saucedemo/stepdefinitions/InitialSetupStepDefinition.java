package co.com.leantech.certification.saucedemo.stepdefinitions;

import co.com.leantech.certification.saucedemo.model.enums.Environments;
import co.com.leantech.certification.saucedemo.util.Log;
import cucumber.api.java.Before;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.pages.Pages;
import org.junit.After;

import static net.thucydides.core.ThucydidesSystemProperty.WEBDRIVER_BASE_URL;

public class InitialSetupStepDefinition {

    @ManagedPages
    public Pages pages;

    @Before
    public void SetStage(){
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Mauricio");
        getEnvironment();
    }

    private void getEnvironment(){
        try {
            Environments environment = Environments.valueOf(System.getProperty("environment"));
            pages.getConfiguration().getEnvironmentVariables().setProperty(WEBDRIVER_BASE_URL.getPropertyName(), environment.getStrUrl());
        }catch (Exception e){
            Log.printError(String.format("The property %s is undefined", WEBDRIVER_BASE_URL));
        }
    }

    @After
    public void finishExecution(){
        Serenity.getWebdriverManager().getCurrentDriver().quit();
    }
}
