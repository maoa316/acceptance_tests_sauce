package co.com.leantech.certification.saucedemo.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/sauceDemo.feature",
        tags = "@e2eSauceDemo",
        glue = "co.com.leantech.certification.saucedemo.stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnerSauceDemo {
}
