package co.com.leantech.certification.saucedemo.stepdefinitions;

import co.com.leantech.certification.saucedemo.model.PersonalDataModel;
import co.com.leantech.certification.saucedemo.questions.Checkout;
import co.com.leantech.certification.saucedemo.questions.Checks;
import co.com.leantech.certification.saucedemo.tasks.*;
import co.com.leantech.certification.saucedemo.util.Constants;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static org.hamcrest.Matchers.containsStringIgnoringCase;

public class LoginSauceDemoStepDefinitions {

    @Given("^than mauricio wants enter the saucedemo website$")
    public void thanMauricioWantsEnterTheSaucedemoWebsite() {
        OnStage.theActorInTheSpotlight().attemptsTo(OpenUp.thePage());
    }

    @When("^enter the system with the username (.*)$")
    public void enterTheSystemWithTheUsername(String strUserName) {
        OnStage.theActorInTheSpotlight().attemptsTo(Login.withTheUser(strUserName));
    }

    @Then("^he see the (.*) text on the current page$")
    public void heSeeTheTextOnTheCurrentPage(String strExpectedResult){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Checks.expectedResults(strExpectedResult)));
    }

    @When("^he want purchase the lowest and highest priced products$")
    public void heWantPurchaseTheLowestAndHighestPricedProducts(List<PersonalDataModel> personalData) {
        OnStage.theActorInTheSpotlight().attemptsTo(Purchase.theProducts(personalData));
    }

    @Then("^he see the Checkout: Complete!$")
    public void heSeeTheCheckoutComplete() {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Checkout.thePurchase(), containsStringIgnoringCase(Constants.CHECKOUT_COMPLETE)));
    }

}
