# README #

This README can you run the automation of saucedemo.

#Prerequisites 

* Java(JDK 1.8)
* Google Chrome last version (90.0.xxx)
* Gradle(5.2) - Optional

## Get the code
git clone https://maoa316@bitbucket.org/maoa316/acceptance_tests_sauce.git


## Use Gradle

Open a command window and run:

    ./gradlew clean test --tests co.com.leantech.certification.saucedemo.runners.RunnerSauceDemo -Denvironment=TEST_SWAGLABS

This runs Cucumber features using Cucumber's JUnit runner. The `@RunWith(Cucumber.class)` annotation on the `RunnerSauceDemo` class tells JUnit to kick off Cucumber.
gradlew is a [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html), which is a recommended way to run Gradle, with a self-container gradle executable.


## View the execution report

The execution report can be consulted in the route

    acceptance_tests_sauce\target\site\serenity\index.html